/*!
 * Module dependencies
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * User schema
 */

const PersonSchema = new Schema({
    name: { type: String, default: '' },
    facebook: { type: String, default: '' },
    instagram: { type: String, default: '' },
    interests: { type: [String], default: [] },
    imageUrl: { type: String, default: '' },
    wasChosen: { type: Boolean, default: false }
});

mongoose.model('Person', PersonSchema);

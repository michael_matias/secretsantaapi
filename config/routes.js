'use strict';
const mongoose = require('mongoose');
const Person = mongoose.model('Person');
/**
 * Module dependencies.
 */
/**
 * Expose
 */

module.exports = function (app) {

  app.get('/', (req, res) => {
      res.status(200).send({'result': 'hello world'})
  });
  app.get('/alreadyAssignedPerson', (req, res) => {
      res.status(200).send(!!req.cookies['assignedPerson']);
  });

  app.post('/secretSantaPerson', async (req, res) => {
      try {
          let code = req.body.code;
          let person = await Person.findOne({code,});
          if (!person) {
              res.status(200).send({result: undefined, message: `Code didn't match our records`})
          } else {
              await Person.update({_id: mongoose.Types.ObjectId(person._id)}, {wasChosen: true});
              res.cookie('assignedPerson', true);
              res.status(200).send({result: person});
          }
      } catch (err) {
          res.status(500).send(err);
      }
  });

  /**
   * Error handling
   */

  app.use(function (err, req, res, next) {
    // treat as 404
    if (err.message
      && (~err.message.indexOf('not found')
      || (~err.message.indexOf('Cast to ObjectId failed')))) {
      return next();
    }
    console.error(err.stack);
    // error page
    res.status(500).send({ error: err.stack });
  });
};


/**
 * Module dependencies.
 */

var express = require('express');
var cors = require('cors');
var compression = require('compression');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

var config = require('./');
/**
 * Expose
 */

module.exports = function (app) {

  // Compression middleware (should be placed before express.static)
  app.use(compression({
    threshold: 512
  }));

  app.use(cookieParser());

  // Static files middleware
  app.use(express.static(config.root + '/public'));

  const whiteListServers = ['http://localhost:3001', 'https://matias-sidi-christmas.herokuapp.com', 'http://sidi.matiaschristmas.com'];
  const corsOptions = {
      origin: (origin, callback) => {
        console.log(origin);
          if (whiteListServers.indexOf(origin) !== -1) {
              callback(null, true)
          } else {
              callback(new Error('Not allowed by CORS'))
          }
      },
      credentials: true
    };
  app.use(cors(corsOptions));

  // bodyParser should be above methodOverride
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
      // look in urlencoded POST bodies and delete it
      var method = req.body._method;
      delete req.body._method;
      return method;
    }
  }));

  // cookieParser should be above session
  app.use(cookieParser());
  app.use(cookieSession({ secret: 'secret' }));
};
